#pragma once

#include <iostream>
#include <vector>
#include <stdio.h>
#include "Unit.hpp"
#include "Cell.hpp"
using namespace std;

class Game;
class PlayerAction;

class Player
{
    public :
        // Constructor declaration
        Player(int a, string b, int e, int _color [3]);
        int id;
        string name;
        int r;
        int g;
        int b;
        vector <Unit*> units; // Liste des unités du joueur
        int money;
        Game* game;
        bool playerTurn(SDL_Event* e); // Tour de jeu du joueur
        void buyUnits(SDL_Event* e);
        bool playerIsDead();

    private :

        void selectUnit(SDL_Event* e); // Le joueur selectionne une de ses unité et commence une action (attaquer ou déplacer)
        PlayerAction* currentAction; // Action en cours (attaquer ou déplacer une unité) 

        // // setters & getters name
        // void setName(string name);
        // string getName();

        // // setters & getters units
        // void setUnits(vector<Unit> units);
        // string getUnits();


        // // setters & getters color
        // void setColor(string color);
        // string getColor();

        // // setters & getters money
        // void setMoney(int money);
};

class PlayerAction {

    public: 

    PlayerAction();
    virtual ~PlayerAction();

    Player* player;
    Unit* attacker; // Unité que le joueur vient de sélectionner parmi ses unités
    virtual void execute();
    virtual void selectTarget(SDL_Event* e);
    virtual bool isSet();
    string type;
    virtual bool animate(); // Animation de l'action
};

class PlayerAttack : public PlayerAction {

    public: 

    PlayerAttack(Player* _player, Unit* _unit);

    Unit* target; // Unité adversaire ciblée par l'attaque

    virtual void execute(); // Éxécute l'action    
    virtual void selectTarget(SDL_Event* e); // Choisie la cible de l'attaque
    virtual bool isSet(); // Renvoie true si la target est définie
    virtual bool animate(); // Animation de l'attack (shot)

    private:
    Shot* shot; // Tir de l'attaque
};

class PlayerMove : public PlayerAction {

    public:

    PlayerMove(Player* _player, Unit* _attacker);
    virtual ~PlayerMove(); // Destructeur

    Cell* destination; // Cellule de destination du déplacement

    virtual void execute(); // Éxécute l'action
    virtual void selectTarget(SDL_Event* e); // Choisie la case de destination
    virtual bool isSet(); // Renvoie true sur la destination est définie
    virtual bool animate(); // Animation du déplacement
};