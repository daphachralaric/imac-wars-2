#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <SDL/SDL_ttf.h>

#pragma once

class Text
{
    public:

    Text( );
    Text(const char* fontPath, float size, SDL_Color color, const char* content);

    TTF_Font *font; // Police de caractère
    SDL_Surface *text; // Contenu du texte
    SDL_Color color; // Couleur du texte
    GLuint texture;

    void displayText(float posX, float posY);
    void deleteText();
};