#ifndef DEF_CELLA
#define DEF_CELLA

#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include "Cell.hpp"
#include <stdio.h>
#include <iostream>

class Game;

class Node {

    public:

        Node(int _x, int _y, Node* _parent);
        ~Node();

        Node* parent; // Node parent      
        int dist(Node* node); // Renvoie la distance de manhattan au node donnée
        int cost(Node* A, Node* B); // Renvoie le coût de la cellule
        bool isNodeEqual(Node* node); // True si le node a les mêmes x y
        bool isInVector(std::vector <Node*> vec); // true si le node est trouvée dans le vector
        int x; // Coordonnées du node
        int y; // Coordonnées du node
};

class Astar {

    public:

    Astar(Cell* _A, Cell* _B, Game* _game); // Astar du point A au point B
    ~Astar();

    std::vector <Node*> visited; // Liste des nodes déjà visités dont on a testé checkChilds()
    std::vector <Node*> tovisit; // lise des nodes dékà visités dont on a PAS testé checkChilds()
    std::vector <Cell*> calculate(); // Calcule le chemin et renvoie une liste de cell

    private:

    Node* A; // Point de départ
    Node* B; // Point d'arrivée
    Game* game; // Partie de jeu
    void checkChilds(Node* node); // Check les 4 cases adjacentes, les met dans <visited> puis trouve le node avec le plus bas cost et le renvoie
    Node* findLowestCost(); // Renvoie le node ayant le cost le plus bas de to_visit
};

#endif
