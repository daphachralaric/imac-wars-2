#pragma once

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <iostream>
#include <vector>
#include <tuple>
#include "Text.hpp"
#include "Texture.hpp"

using namespace std;

class Button
{
    public:

    Button(
        int _posX,
        int _posY,
        float _height,
        float _width,
        int _colorStandard [3],
        int _colorHover [3],
        int _colorDown [3],
        const char* _fontPath,
        float _size,
        SDL_Color _color,
        const char* _content
    );

    Text text; // Texte du bouton
    int posX;
    int posY;
    float height;
    float width;
    int colorStandard [3]; // Couleur standard du bouton RVB
    int colorHover [3]; // Couleur on hover RVB
    int colorDown [3]; // Couleur on click Down RVB
    void displayButton(SDL_Event* e);
    void (*onClick)(); // Fonction à appelé au click
    
    private :

    bool isHovered(SDL_Event* e); // revoie true quand le bouton est sur le bouton
    bool isClickedUp(SDL_Event* e); // devient true quand le bouton est cliqué (Mouse Up)
    bool isClickedDown(SDL_Event* e); // devient true quand le bouton est cliqué (Mouse Down)  
};

class Interface // Ensemble de boutons et de textures qui doivent être affichés ensemble
{
    public:

    Interface ();

    string name;
    vector <Button*> buttonsList; // Liste des boutons de cette interface
    vector < tuple<Texture*, int, int, int, int> > texturesList; // Liste des textures X, Y, W, H
    vector < tuple<Sprite*, int, int, int> > spritesList; // Liste dees textures

    void displayInterface(SDL_Event* e);
};