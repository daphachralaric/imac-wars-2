#pragma once

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>

class Texture
{
    public:

    Texture( );
    Texture(const char* path);

    GLuint texture; // Objet OpenGL de la texture
    SDL_Surface* image; // Image de la texture
    const char* path; // Chemin vers l'image
	bool isTexture; //true si on peut se protéger

    void loadTexture(); // Charger la texture
    void displayTexture(float posX, float posY, float width, float height, SDL_Event* e); // Afficher la texture
    void deleteTexture(); // Supprime la texture

    private :

    bool isHovered(float posX, float posY, float width, float height, SDL_Event* e);
    bool isClickedUp(float posX, float posY, float width, float height, SDL_Event* e);
    bool isClickedDown(float posX, float posY, float width, float height, SDL_Event* e);
};

class Sprite : public Texture
{
    public:

    Sprite( );
    Sprite(const char* _path, int _spriteW = 32, int _spriteH = 32);

    float spriteW; // Largeur du sprite à afficher
    float spriteH; // Hauteur du sprite
    int timeCount; // Compteur de temps pour l'animation
    int frameIndex; // Frame actuellement affichée

    void displaySprite(int posX, int posY, int size, int speed = 6); // Afficher le sprite animé, speed = nombre de frames avant de changer
    void oui();
};

class Shot : public Sprite
{
    public :

    Shot(const char* _path, int _spriteW, int _spriteH, float _posX, float _posY, float _tarX, float _tarY, int _size, float _speed);
    ~Shot(); // Destructeur

    float posX;
    float posY;
    float tarX; // Coordonnées cible X
    float tarY; // Coordonnées cible Y
    float size;
    double mX, mY;
    float speed;
    bool finished = false; // Devient true quand le trajet est fini

    void displayShot(); // Affiche sprite qui se déplace

    private :

    void set();
};