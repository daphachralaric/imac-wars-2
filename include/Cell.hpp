#ifndef DEF_CELL
#define DEF_CELL

#include "Unit.hpp"
#include "Texture.hpp"

class Cell {

    public:

        Cell (int _x, int _y, Texture* _texture);
        void displayCell(int size, SDL_Event* e);
        void deleteUnit();
        int x;
        int y;
        Texture* texture;
        void setUnit(Unit* _unit); // Met une unité dans la case
        bool isClicked(SDL_Event* e);
        Unit* unit; // pointer
        const char* type[6];
        bool moveThrough(); //une unité peut bouger au travers
        bool attackThrough(); //une unité peut attaquer au travers
        void setType(); //quel type de texture sur la cellule

    private:

        bool isHovered(SDL_Event* e);        
};

#endif
