#ifndef DEF_UNIT
#define DEF_UNIT

#include "Texture.hpp"

class Player;
class Cell; // forward declaration, because cell includes unit which includes cell etc.

class Unit {

    public:

    Unit();
    Unit(Cell* _cell, Sprite* _sprite, Player* _owner);
    ~Unit();
    void displayUnit();
    void attackUnit(Unit* target);
    void moveUnit(Cell* destination);
    void deleteUnit();
    Cell* cell; // reference because it can’t be nullptr
    Player* owner; // Joueur qui détient l'unité
    float life;
    float strength;
    int def;
    int range;
    int mobility;
    int cost;
    Sprite* sprite;
    Unit* isClicked(SDL_Event* e); // Renvoie l'unité si elle est cliquée
    void resetUnitTurn(); // réinitialise hasMoved et hasAttack, à appelé à la toute fin du tour du joueur
    bool isDead(); // true si l'unité est morte
    bool isTurnDone(); // true si hasMoved et hasAttack sont
    bool hasMoved; // true si l'unité s'est déjà déplacée pendant ce tour
    bool hasAttack; // true si l'unité a déjà attaqué pendant ce tour


    private :

    Shot* shot; // animation du tir
};

class Fantassin : public Unit {
    public:

    Fantassin(Cell* _cell, Sprite* _sprite, Player* _owner);
    ~Fantassin();

    int life = 7;
    int range = 1;
    int mobility = 1;
    int strength = 4;
    int def = 2;
    int cost = 5;  

};

class Archer : public Unit {
    public:

    Archer(Cell* _cell, Sprite* _sprite, Player* _owner);
    ~Archer();

    int life = 5;
    int range = 3;
    int mobility = 1;
    int cost = 5;  
    int strength = 4;
    int def = 0;
};

class Knight : public Unit {
    public:

    Knight(Cell* _cell, Sprite* _sprite, Player* _owner);
    ~Knight();

    int life = 11;
    int range = 1;
    int mobility = 2;
    int cost = 10;
    int strength = 6;
    int def = 2;
};

#endif
