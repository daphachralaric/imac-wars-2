#pragma once

#include <vector>
#include "Text.hpp"
#include "Texture.hpp"
using namespace std;

extern vector <Texture*> loadedTextures;
extern vector <Text*> loadedTexts;

/* Dimensions initiales et titre de la fenetre */
[[maybe_unused]] static const int MAP_SIZE = 10; // TAILLE DE LA MAP (HAUTEUR ET LARGEUR)
[[maybe_unused]] static const unsigned int WINDOW_WIDTH = 1280;
[[maybe_unused]] static const unsigned int WINDOW_HEIGHT = 720;
[[maybe_unused]] static const char WINDOW_TITLE[] = "IMAC WARS 2"; // Titre de la fenêtre

void initTextures();

/* Joueurs */
[[maybe_unused]] static const int NB_PLAYERS = 2;

/* TEXTURES */
// Summer cells
[[maybe_unused]] extern Texture summer_dirt;
[[maybe_unused]] extern Texture summer_grass;
[[maybe_unused]] extern Texture summer_stone;
[[maybe_unused]] extern Texture summer_water;

// Autumn cells
[[maybe_unused]] extern Texture autumn_dirt;
[[maybe_unused]] extern Texture autumn_grass;
[[maybe_unused]] extern Texture autumn_stone;
[[maybe_unused]] extern Texture autumn_water;

// Autumn cells
[[maybe_unused]] extern Texture winter_dirt;
[[maybe_unused]] extern Texture winter_grass;
[[maybe_unused]] extern Texture winter_stone;
[[maybe_unused]] extern Texture winter_water;

// Units
[[maybe_unused]] extern Sprite tank;
[[maybe_unused]] extern Sprite soldier;

// Inteface
[[maybe_unused]] extern Sprite cellCursor;

/* FONTS */
[[maybe_unused]] static const char* SummitAttack = "fonts/SummitAttack.ttf";
