#pragma once

using namespace std;

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "Game.hpp"
#include "Texture.hpp"
#include "Text.hpp"
#include "Interface.hpp"
#include "Const.hpp"
#include "Unit.hpp"
#include "Player.hpp"
#include "Cell.hpp"