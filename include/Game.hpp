#pragma once

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <vector>
#include "Cell.hpp"
#include "Const.hpp"
#include "Player.hpp"

class Game
{
    public:

    Game();

    Cell* map[MAP_SIZE][MAP_SIZE]; // Carte de jeu
    // Player* players[NB_PLAYERS]; // Liste des joueurs
    vector <Player*> players; // Liste des joueurs

    void startGame(); // Lancer la partie
    void incTurnNb(); // Augmente l'indice du tour de jeu de 1
    void endGame(); // Terminer la partie
    int playGame(SDL_Event* e); // Appeler en continue quand la partie est en train de jouer
    void setPlayer(Player* player);
    bool isWinner(); // Renvoie true si il n'y a plus qu'un joueur en vie
    void initGame(); // Initialise la game (met les joueurs, leurs unités)
    bool isGameInit(); // true si isInit == true
    void incPlayerIndex(); // Augmente playerIndex ou remet à 0 si arrivé au bout de le liste de joueur
    void cleanGame(); // Remet là partie à 0

    private:

    Cell* markov(int* k, int x, int y, float tabProba[9]);
    int playerIndex; // Index du joueur dont le tour est en cours
    int turnNb; // Indice du tour de jeu courant
    bool isInit; // true si la game est déjà initialisées
    void initMap(); // Initialise la map
    void displayMap(SDL_Event* e); // Affiche la carte
};