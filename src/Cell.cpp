#include "Cell.hpp"
#include "Const.hpp"
#include "Unit.hpp"
#include "Player.hpp"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>
#include <iostream>

using namespace std;

Cell::Cell(int _x, int _y, Texture* _texture){
    x = _x;                 //coordonnées de la cellule
    y = _y;
    texture = _texture;     //texture de la cellule
    unit = 0;               //s'il y a une unité ou non dans la cellule
};

void Cell::deleteUnit() {
    cout << "Cell::deleteUnit" << endl;
    // delete this->unit;
    this->unit = nullptr;
};

void Cell::displayCell(int size, SDL_Event* e) {
    // Afficher la texture du sol
	texture->displayTexture(x*size, y*size, size, size, e);

    // Afficher l'unité si existe
    if (unit != 0) {
        int s = WINDOW_HEIGHT/MAP_SIZE;
        // CASE DE COULEUR DU JOUEUR
        glPushMatrix();
            glBegin(GL_QUADS);
                glColor4ub(unit->owner->r, unit->owner->g, unit->owner->b, 64);
                glVertex2f(x*s,y*s);
                glVertex2f(x*s+s,y*s);
                glVertex2f(x*s+s,y*s+s);
                glVertex2f(x*s,y*s+s);
                glColor4ub(255, 255, 255, 255);
            glEnd();
        glPopMatrix();
        unit->displayUnit();
    }

    // Curseur si hover
    if (isHovered(e)) {
        cellCursor.displaySprite(x*size, y*size, WINDOW_HEIGHT/MAP_SIZE, 10);
    }

    // Unit hovered
    if (unit != 0 && isHovered(e)) {

        // int caseX = x;
        // int caseY = y;
        // // Cercle autour de la case pointée
        //     int range = 4;
        //     Sprite hoverDown("img/sprite.png");
        //     for (int i = -range; i <= range; i++)
        //     {
        //         for (int j = -range; j <= range; j++)
        //         {
        //             if ((i+j) <= range && (i+j) >= -range && (i-j) <= range && (i-j) >= -range && !(i==0 && j==0))
        //             {
        //                 hoverDown.displaySprite(
        //                         ((WINDOW_HEIGHT/MAP_SIZE)*(caseX))+(i*(WINDOW_HEIGHT/MAP_SIZE)),
        //                         ((WINDOW_HEIGHT/MAP_SIZE)*(caseY))+(j*(WINDOW_HEIGHT/MAP_SIZE)),
        //                         (WINDOW_HEIGHT/MAP_SIZE)
        //                     );
        //             }
        //         }
        //     }
    }

};

void Cell::setUnit(Unit* _unit) {
    //attribue une unité à une cellule
    unit = _unit;
}


// Détecter si la cellule est hover
bool Cell::isHovered(SDL_Event* e) {
    int px, py;
    SDL_GetMouseState(&px, &py);
    if (px > x*(WINDOW_HEIGHT/MAP_SIZE) && px < x*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE) && py > y*(WINDOW_HEIGHT/MAP_SIZE) && py < y*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE))
    {
        return true;
    } else {
        return false;
    }
    // if (e != NULL && e->type == SDL_MOUSEMOTION && e->button.x > x*(WINDOW_HEIGHT/MAP_SIZE) && e->button.x < x*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE) && e->button.y > y*(WINDOW_HEIGHT/MAP_SIZE) && e->button.y < y*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE))
    // {
    //     cout << "nique la bac "<< endl;
    //     return true;
    // } else {
    //     return false;
    // }
}

// Détecter si la cellule est cliquée
bool Cell::isClicked(SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEBUTTONUP && e->button.x > x*(WINDOW_HEIGHT/MAP_SIZE) && e->button.x < x*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE) && e->button.y > y*(WINDOW_HEIGHT/MAP_SIZE) && e->button.y < y*(WINDOW_HEIGHT/MAP_SIZE)+(WINDOW_HEIGHT/MAP_SIZE))
    {
        return true;
    } else {
        return false;
    }
}

void Cell::setType() {
	if (this->texture == &summer_water || this->texture == &autumn_water || this->texture == &winter_water)
		*this->type = "water";
	if (this->texture == &summer_stone || this->texture == &autumn_stone || this->texture == &winter_stone)
		*this->type = "stone";
    if (this->texture == &summer_grass || this->texture == &autumn_grass || this->texture == &winter_grass)
		*this->type = "grass";
}


bool Cell::moveThrough() {
	//l'obstacle est traversable par une unité
    this->setType();
	return (*this->type == "grass");
};

bool Cell::attackThrough() {
	//une unité peut attacker à travers l'obstacle
    this->setType();
	return *this->type != "stone";
};