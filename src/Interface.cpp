#include "../include/Interface.hpp"
#include "../include/Text.hpp"

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <string>

using namespace std;

Button::Button(
        int _posX,
        int _posY,
        float _height,
        float _width,
        int _colorStandard[3],
        int _colorHover[3],
        int _colorDown[3],
        const char* _fontPath,
        float _size,
        SDL_Color _color,
        const char* _content
    ) {
        posX = _posX;
        posY = _posY;
        height = _height;
        width = _width;
        memcpy ( colorStandard, _colorStandard, sizeof(colorStandard));
        memcpy ( colorHover, _colorHover, sizeof(colorHover));
        memcpy ( colorDown, _colorDown, sizeof(colorDown));
        Text t(_fontPath, _size, _color, _content);
        text = t;
    };

void Button::displayButton(SDL_Event* e) {
    int c[3]; // Couleur courante
    isClickedUp(e);
    if (isHovered(e))
    {
        c[0] = colorHover[0];
        c[1] = colorHover[1];
        c[2] = colorHover[2];
    } else if (isClickedDown(e)){
        c[0] = colorDown[0];
        c[1] = colorDown[1];
        c[2] = colorDown[2];
    } else {
        c[0] = colorStandard[0];
        c[1] = colorStandard[1];
        c[2] = colorStandard [2];
    }

    // cout << "Bouton :" << posX << " " << posY << endl;
    glPushMatrix();
        glBegin(GL_QUADS);
            glColor3ub(c[0], c[1], c[2]);
            glVertex2f(posX,posY);
            glVertex2f(posX+width,posY);
            glVertex2f(posX+width,posY+height);
            glVertex2f(posX,posY+height);
        glEnd();
    glPopMatrix();

    // Coordonnées du texte
    float tX = posX+width/2-text.text->w/2;
    float tY = posY+height/2-text.text->h/2;
    text.displayText(tX, tY);
}

bool Button::isHovered(SDL_Event* e) {
    // cout << "buttonHover " << e->button.x << " " << e->button.y << endl;
    int px, py;
    SDL_GetMouseState(&px, &py);
    if (px > posX && px < posX+width && py > posY && py < posY+height)
    {
        return true;
    } else {
        return false;
    }
}

bool Button::isClickedUp(SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEBUTTONUP && e->button.x > posX && e->button.x < posX+width && e->button.y > posY && e->button.y < posY+height)
    {
        // cout << "Button CLICKED UP\n";
        onClick();
        return true;
    } else {
        return false;
    }
}

bool Button::isClickedDown(SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEBUTTONDOWN && e->button.x > posX && e->button.x < posX+width && e->button.y > posY && e->button.y < posY+height)
    {
        // cout << "CLICKED DOWN\n";
        return true;
    } else {
        return false;
    }
}

Interface::Interface () {
    cout << "Interface created" << endl;
}

// Affiche l'interface
void Interface::displayInterface (SDL_Event* e) {
    // cout << "displayInterface" << endl;
    // TEXTURES
    for (size_t i = 0; i < texturesList.size(); i++)
    {
        int x = get<1>(texturesList[i]);
        int y = get<2>(texturesList[i]);
        int w = get<3>(texturesList[i]);
        int h = get<4>(texturesList[i]);
        get<0>(texturesList[i])->displayTexture(x, y, w, h, e);
    }

    // BOUTONS
    for (size_t i = 0; i < buttonsList.size(); i++)
    {
        buttonsList[i]->displayButton(e);
    }
    
} 