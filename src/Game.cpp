#include "../include/Game.hpp"
#include "../include/Cell.hpp"
#include "../include/Const.hpp"
#include "../include/Player.hpp"
#include <iostream>
#include <cstdlib>

using namespace std;

//structure d'une partie de jeu

Game::Game() {
    turnNb = 0;
    playerIndex = 0;
};

void Game::incTurnNb() {
    //nombre de tours
    turnNb++;
    incPlayerIndex();
};

Cell* Game::markov(int* k, int x, int y, float tabProba[9]) {
    float p = (float)rand() /  (float)RAND_MAX;
    //cout << "Markov random :" << p << end

    //si k = 1, la texture est de l'herbe
    if(*k == 1){
        Cell* c = new Cell(x, y, &summer_grass);
        cout << "coordonnées de c :" << c->x << ", " << c->y << endl;
        if(p < tabProba[0]){
            *k = 1;             //chance qu'il y ait de l'herbe après une tuile d'herbe
        }
        if(tabProba[0] <= p && p < 1 - tabProba[1]){
            *k = 2;             //chance qu'il y ait de la pierre après une tuile d'herbe
        }
        if(p >= 1 - tabProba[2]){
            *k = 3;             //chance qu'il y ait de l'eau après une tuile d'herbe
        }
        return c;
        cout << "p : " << p << endl;
    }

    //si k = 2, la texture est de la pierre 
    if(*k == 2){
        Cell* c = new Cell(x, y, &summer_stone);
        if(p < tabProba[3]){
            *k = 1;
        }
        if(tabProba[3] <= p && p < 1 - tabProba[4]){
            *k = 2;
        }
        if(p >= 1 - tabProba[5]){
            *k = 3;
        }
        return c;
    }

    //si k = 3, la texture est de l'eau
    if(*k == 3){
        Cell* c = new Cell(x, y, &summer_water);
        if(p < tabProba[6]){
            *k = 1;
        }
        if(tabProba[6] <= p && p < 1 - tabProba[7]){
            *k = 2;
        }
        if(p >= 1 - tabProba[8]){
            *k = 3;
        }
        return c;
    }
};
    
void Game::initGame() {
    // Génération de la map
    initMap();
    cout << "Game::initGame() started" << endl;
    for (int i = 0; i < NB_PLAYERS; i++)
    {
        // Couleur du joueur
        int c[3] = {((i+1)%2)*255, 0, ((i)%2)*255};
        
        Player* newplayer = new Player(i, "Joueur"+to_string(i), 0, c);
        Unit* unit = new Fantassin(map[5][5+i], &soldier, newplayer);
        unit->life = 100;
        unit->strength = 0.5;
        newplayer->units.push_back(unit);
        setPlayer(newplayer);
        cout << " »  Added new Player : " << newplayer->name << endl;
    };    
};

void Game::cleanGame() {
    // Suppression des joueurs
    for (int i = 0; i < players.size(); i++)
    {
        delete players[i];
    };

    turnNb = 0;
    playerIndex = 0;
    isInit = false;

    for (int i = 0; i < MAP_SIZE; i++)
    {
        for (int j = 0; j < MAP_SIZE; j++)
        {
            delete map[i][j];
        };
    };
};

void Game::initMap() {
    //initialise la map au début d'une partie
    int k = 1;
    float probaTiles[9] =
                {0.9, 0.05, 0.05, // SUM = 1
                  0.6, 0.2, 0.2, // SUM = 1
                  0.7, 0.1, 0.2}; // SUM = 1
     for (int i = 0; i < MAP_SIZE; i++)
     {
        for (int j = 0; j < MAP_SIZE; j++)
        {
            Cell* c = markov(&k, i, j, probaTiles);
            // cout << "i j : " << i << " " << j << endl;
            // cout << "iniMap : " << c->x << " " << c->y << " " << " " << c->texture->path << endl;
            map[i][j] = c;
        }
    }
};

void Game::incPlayerIndex() {
    if (playerIndex < (int)players.size() - 1)
    {
        playerIndex++;
    } else {
        playerIndex = 0;
    }
    cout << "=> playerIndex : " << playerIndex << endl; 
};

int Game::playGame(SDL_Event* e) {
    // cout << "playGame" << endl;
    // AFFICHAGE DE LA MAP
    displayMap(e);
    //players[playerIndex]->buyUnits(e);


    // TOUR DES JOUEURS
    if (players[playerIndex]->playerTurn(e))
    {
        return 1; // La partie continue
    }
    else if (!isWinner())
    {
        incTurnNb(); // On passe au tour suivant
        return 1; // La partie continue
    } else {
        cout << "La partie est finie !" << endl;
        return 0;
    }
};

void Game::displayMap(SDL_Event* e) {
    //cout << "entrée display :" << map[9][9]->texture->path << endl;
    int size = WINDOW_HEIGHT/MAP_SIZE;
    for (int i = 0; i < MAP_SIZE; i++) {
        for (int j = 0; j < MAP_SIZE; j++)
        {
            //cout << "Entré dans displayMap" << endl;
            //cout << map[i][j]->x << ", "<< map[i][j]->y << endl;
            map[i][j]->displayCell(size, e);
        }
    }
};

void Game::setPlayer(Player* player) {
    cout << "SET PLAYER IN GAME" << endl;
    players.push_back(player);
    player->game = this;
};

bool Game::isWinner() {
    uint alive = 0;
    for (uint i = 0; i < players.size(); i++) {
        if (players[i]->playerIsDead())
        {
            alive++;   
        }
    };
    // cout << " » " << alive << " joueurs en vie" << endl;
    if (alive < 2)
    {
        // cout << "LA PARTIE EST FINIE" << endl;
        return true;
    } else {
        return false;
    };
};

bool Game::isGameInit() {
    if (isInit) {
        return true;
    } else {
        return false;
    }
};