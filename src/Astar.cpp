#include "../include/Astar.hpp"
#include "../include/Const.hpp"
#include "../include/Cell.hpp"
#include "../include/Game.hpp"
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

/* NODE ------------------------------- */

Node::Node(int _x, int _y, Node* _parent) {
	x = _x;
	y = _y;
	parent = _parent;
};
Node::~Node(){};

int Node::dist(Node* node) {
	// cout << "node->x : " << node->x << endl;
	// cout << "node->y : " << node->y << endl;
	// cout << "x : " << x << endl;
	// cout << "y : " << y << endl;
	int dist = (abs(node->x - x) + abs(node->y - y));
	//cout << " » dist() -> " << dist << endl;
	return dist;
};

int Node::cost(Node* A, Node* B) {
	int c = 2*dist(A) + dist(B);
	cout << " » Cost is " << c << endl;
	return c;
};

bool Node::isNodeEqual(Node* node) {
	if (node->x == x && node->y == y)
	{
		return true;
	} else {
		return false;
	}
};

bool Node::isInVector(std::vector <Node*> vec) {
	for (uint i = 0; i < (uint)vec.size(); i++)
	{
		if (isNodeEqual(vec[i]))
		{
			return true;
		}
	}
	return false;
};

/* ASTAR --------------------------- */

Astar::Astar(Cell* _A, Cell* _B, Game* _game) {
	cout << " » NEW ASTAR " << endl;
	A = new Node(_A->x, _A->y, NULL);
	B = new Node(_B->x, _B->y, NULL);
	game = _game;
};
Astar::~Astar() {

};

void Astar::checkChilds(Node* node) {
	int m [4][2] = {{0, -1}, {-1, 0}, {0, 1}, {1, 0}};
		
	for (int i = 0; i < 4; i++)
	{
		// Si la cellule n'est pas obstruée
		if (game->map[node->x+m[i][0]][node->y+m[i][1]]->moveThrough())
		{
			// Si le node est déja visité
			if (node->isInVector(visited))
			{
				cout << "A* : la case est déjà dans visited" << endl;
				continue;
			} 
			// Si le node est déjà créé
			else if (node->isInVector(tovisit)) {
				// Utiliser le node existant
				cout << "A* : la case est déjà dans tovisit" << endl;
				/// ??????
				continue;
			}
			// Le le node est le point d'arrivée
			else if (node->isNodeEqual(B))
			{
				cout << " » point B trouvé !" << endl;
				B->parent = node;
				visited.push_back(B);
			}
			else // Sinon c'est que le node n'existe pas encore, donc on le crée
			{
				Node* newNode = new Node(node->x+m[i][0], node->y+m[i][1], node);
				tovisit.push_back(newNode);
				cout << "Node " << newNode->x <<" "<< newNode->y<< " ajouté à tovisit" << endl;
			}
		}
		else {
			cout << "Case obstruée" << endl;
			continue;
		}
	}
	visited.push_back(node);
	for (uint i = 0; i < (uint)tovisit.size(); i++)
	{
		if (tovisit[i]->isNodeEqual(node))
		{
			cout << " × node erase from tovisit : " << node->x << " " << node->y << endl;
			tovisit.erase(tovisit.begin()+i);
		}
		
	};	
	cout << "Node " << node->x <<" "<< node->y<< " ajouté à visited" << endl;
};

std::vector <Cell*> Astar::calculate() {
	// Initialisation du premier node
	cout << " » Calculate Astar ..." << endl;
	checkChilds(A);
	cout << "checkChilds(A) done" << endl;

	// Tant qu'on a pas atteint B on cherche dans les enfants, des enfants ... jusqu'à le trouver
	while (!B->isInVector(visited))
	{
		cout << " --- Boucle A* --- " << endl;
		cout << "visited.size() = " << visited.size() << endl;
		Node* currentNode = findLowestCost();
		cout << "!!! current node : " << currentNode->x << " " << currentNode->y << endl;
		checkChilds(currentNode);
	};

	// Création du path
	std::vector <Cell*> path;
	Node* parent = B->parent;
	path.push_back(game->map[B->x][B->y]);
	while (parent != NULL)
	{
		path.push_back(game->map[parent->x][parent->y]);
		cout << " » cellule "<< game->map[parent->x][parent->y]->x << " " << game->map[parent->x][parent->y]->y << " ajoutée au path" << endl;
		parent = parent->parent;
	};
	return path;	
};

Node* Astar::findLowestCost() {
	Node* lowest = A;
	for (int i = 0; i < (int)tovisit.size(); i++)
	{
		cout << "Tour de findLowestCost : " << i << endl;
		// cout << "tovisit[" << i << "] " << tovisit[i]->x << " " << tovisit[i]->y << endl;
		// cout << "tovisit[i]->cost(A, B) " << tovisit[i]->cost(A, B) << endl;
		
		if (tovisit[i]->cost(A, B) < lowest->cost(A, B))
		{
			lowest = tovisit[i];
			cout << "-> New lowest is " << lowest->x << " " << lowest->y << endl;
		} else {
			cout << "-> No lowest change" << endl;
		}
	}
	cout << " // Lowest cost return : " << lowest->x << " " << lowest->y << endl; 
	return lowest;
};