#include "../include/Cell.hpp"
#include "../include/Unit.hpp"
#include "../include/Const.hpp"
#include "../include/Player.hpp"
#include "../include/Astar.hpp"
#include <iostream>
#include <math.h>

Unit::Unit(Cell* _cell, Sprite* _sprite, Player* _owner){
	cell = _cell;
    sprite = _sprite;
    cell->setUnit(this);
    hasMoved = false;
    hasAttack = false;
    owner = _owner;
};

Unit::~Unit() {
    cout << "Deleting Unit from ~Unit();" << endl;
}


Fantassin::Fantassin(Cell* _cell, Sprite* _sprite, Player* _owner) : Unit(_cell, _sprite, _owner) {
	cell = _cell;
    sprite = _sprite;
    cell->setUnit(this);
    hasMoved = false;
    hasAttack = false;
    owner = _owner;
};

Fantassin::~Fantassin() {}

Archer::Archer(Cell* _cell, Sprite* _sprite, Player* _owner) : Unit(_cell, _sprite, _owner){
	cell = _cell;
    sprite = _sprite;
    cell->setUnit(this);
    hasMoved = false;
    hasAttack = false;
    owner = _owner;
};

Archer::~Archer() {}

Knight::Knight(Cell* _cell, Sprite* _sprite, Player* _owner) : Unit(_cell, _sprite, _owner){
	cell = _cell;
    sprite = _sprite;
    cell->setUnit(this);
    hasMoved = false;
    hasAttack = false;
    owner = _owner;
};

Knight::~Knight() {}


void Unit::displayUnit() {
	//affiche l'unité
    int s = WINDOW_HEIGHT/MAP_SIZE;

	sprite->displaySprite(cell->x*s, cell->y*s, s);
};

bool Unit::isTurnDone() {
    if (hasMoved && hasAttack) {
        return true;
    } else {
        return false;
    }
};

void Unit::attackUnit(Unit* target) {
    cout << "Unit attacking !" << endl;
    // Infliger des dégats à la cible
    cout << "UNIT STATS L S : " << life << " " << strength << endl;
    target->life -= strength * life;

    if (target->isDead())
    {
        cout << "Target is dead" << endl;
        cout << "size : " << target->owner->units.size() << endl;
        for (uint i = 0; i < target->owner->units.size(); i++)
        {
            cout << "puuuuute" << endl;
            if (target->owner->units[i] == target)
            {
                cout << "target->owner->units[i] avant erase : " << target->owner->units[i] << endl;
                target->owner->units.erase(target->owner->units.begin() + i);
                cout << "erase done"<< endl;
                cout << "target->owner->units[i] après erase : " << target->owner->units[i] << endl;
                // cout << "target->owner->units.begin()+i : " << target->owner->units[i] << endl;
                target->cell->deleteUnit();
                // delete target;
                cout << "Unit removed" << endl;
            }
            cout << "Unit non trouvé" << endl;
        }  
    } else {
        //La cible réplique
        life -= target->strength * target->life;

        cout << "Unit life : " << life <<endl;
        cout << "Target life : " << target->life << endl;
    }
	hasAttack = true;

    cout << "Fin de Unit::attackUnit()" << endl;
}

bool Unit::isDead() {
    if (life > 0)
    {
        return false;
    } else {
        cout << "Unit::isDead() returned true" << endl;
        return true;
    }
}
	
void Unit::moveUnit(Cell* destination) {
    // Astar* path = new Astar(cell, destination, owner->game);
    // path->calculate();
    destination->unit = this;
    this->cell->setUnit(nullptr);
    this->cell = destination;
    hasMoved = true;
}

void Unit::deleteUnit() {
    this->cell->deleteUnit();
}

Unit* Unit::isClicked(SDL_Event* e) {
    if (this->cell->isClicked(e))
    {
        // printf("Unit clicked\n");
        return this;
    } else {
        return NULL;
    }
}

void Unit::resetUnitTurn() {
    printf("resetUnitTurn\n");
    hasMoved = false;
    hasAttack = false;
}