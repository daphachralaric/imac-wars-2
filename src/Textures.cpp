#include <iostream>
#include <stdio.h>
#include <math.h> 
#include "../include/Texture.hpp"
#include "../include/Const.hpp"
using namespace std;

// Constructeurs
Texture::Texture(const char* _path) {
    path = _path;
    loadTexture();
};

Texture::Texture( ) {
    path = "";
};

//Default
Sprite::Sprite( ) {
    path = "";
    spriteW = 32;
    timeCount = 0;
    frameIndex = 0;
};

Sprite::Sprite(const char* _path, int _spriteW, int _spriteH) {
    path = _path;
    spriteW = _spriteW;
    timeCount = 0;
    frameIndex = 0;
    if (!isTexture) {
        loadTexture();
    }
};
//

void Texture::loadTexture() {
    // Charger l'image
    image = IMG_Load(path);

    glGenTextures(1, &(texture));

    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    isTexture = true;

    loadedTextures.push_back(this);
    cout << "loadedTextures : " << loadedTextures.size() << endl;
}

void Texture::displayTexture(float posX, float posY, float width, float height, SDL_Event* e) {
    isClickedDown(posX, posY, width, height, e);

    glPushMatrix();
        // Permet de superposer les textures
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, texture);
                glBegin(GL_QUADS);
                    glTexCoord2f(0,0);
                    glVertex2f( posX , posY);

                    glTexCoord2f(1,0);
                    glVertex2f( posX+width, posY);

                    glTexCoord2f(1,1);
                    glVertex2f( posX+width , posY+height);

                    glTexCoord2f(0,1);
                    glVertex2f( posX , posY+height);
                glEnd();
            glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

void Texture::deleteTexture() {
    glDeleteTextures(1, &texture);
    SDL_FreeSurface(image);
    cout << "Deleted " << path << endl;
}

void Sprite::displaySprite(int posX, int posY, int size, int speed) {

    int nbFrames = int(image->w / spriteW); // Nombre total de frames du sprite

    // Incémenter à chaque frame
    timeCount++;
    
    // Gestion du timing de l'animation
    if (timeCount%speed == 0 && frameIndex < nbFrames - 1) {
        frameIndex++;
    } else if (timeCount%speed == 0 && frameIndex == nbFrames -1) {
        frameIndex = 0;
    }

    // Afficher une frame du sprite
    float X0 = (frameIndex * spriteW) / image->w;
    float X1 = ((frameIndex * spriteW) + spriteW ) / (image->w);

    glPushMatrix();
        // Permet de superposer les textures
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, texture);
                glBegin(GL_QUADS);

                    glTexCoord2f(X0, 0);
                    glVertex2f( posX , posY);

                    glTexCoord2f(X1, 0);
                    glVertex2f( posX+size, posY);

                    glTexCoord2f(X1, 1);
                    glVertex2f( posX+size , posY+size);

                    glTexCoord2f(X0, 1);
                    glVertex2f( posX , posY+size);

                glEnd();
            glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

bool Texture::isHovered(float posX, float posY, float width, float height, SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEMOTION && e->button.x > posX && e->button.x < posX+width && e->button.y > posY && e->button.y < posY+height)
    {
        return true;
    } else {
        return false;
    }
}

bool Texture::isClickedUp(float posX, float posY, float width, float height, SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEBUTTONUP && e->button.x > posX && e->button.x < posX+width && e->button.y > posY && e->button.y < posY+height)
    {
        // cout << "CLICKED UP\n";
        return true;
    } else {
        return false;
    }
}

bool Texture::isClickedDown (float posX, float posY, float width, float height, SDL_Event* e) {
    if (e != NULL && e->type == SDL_MOUSEBUTTONDOWN && e->button.x > posX && e->button.x < posX+width && e->button.y > posY && e->button.y < posY+height)
    {
        // cout << "CLICKED DOWN\n";
        return true;
    } else {
        return false;
    }
};

Shot::Shot(const char* _path, int _spriteW, int _spriteH, float _posX, float _posY, float _tarX, float _tarY, int _size, float _speed) {
    path = _path;
    spriteW = _spriteW;
    timeCount = 0;
    frameIndex = 0;
    if (!isTexture) {
        loadTexture();
    }
    spriteH = _spriteH;
    posX = _posX;
    posY = _posY;
    tarX = _tarX;
    tarY = _tarY;
    size = _size;
    speed = _speed;
    set();
}

Shot::~Shot() {
    cout << "~Shot()" << endl;
    deleteTexture();
}

void Shot::displayShot() {
    if ((tarX - posX) > 1 || (tarY - posY) > 1)
    {
        // cout << "mX , mY: " << mX << " , " << mY << endl; 
        posX += mX;
        posY += mY;
        // cout << "shot : " << posX << " " << posY << endl;
        displaySprite(posX, posY, size, 6);
   }
   else {
       finished = true;
   }
};

void Sprite::oui() {
    cout << "OUI" << endl;
}

void Shot::set() {
    double vx = tarX - posX;
    double vy = tarY - posY;
    double norm = sqrt(pow(vx, 2) + pow(vy, 2));
    mX = (vx/norm)*speed;
    mY = (vy/norm)*speed;
}