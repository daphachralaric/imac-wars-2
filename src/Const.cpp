#include <vector>
#include "Text.hpp"
#include "Texture.hpp"
#include "Unit.hpp"
#include "../include/Const.hpp"
#include <iostream>
using namespace std;

vector <Texture*> loadedTextures;
vector <Text*> loadedTexts;

// Summer cells
Texture summer_dirt;
Texture summer_grass;
Texture summer_stone;
Texture summer_water;

// Autumn cells
Texture autumn_dirt;
Texture autumn_grass;
Texture autumn_stone;
Texture autumn_water;

// Autumn cells
Texture winter_dirt;
Texture winter_grass;
Texture winter_stone;
Texture winter_water;

// Units
Sprite tank;
Sprite soldier;

// Interface
Sprite cellCursor;

void initTextures() {
    printf("initTextures START\n");
    /* TEXTURES */
    // Summer cells
    summer_dirt.path = "img/summer/summer_dirt.png";
    summer_dirt.loadTexture();
    summer_stone.path = "img/summer/summer_stone.png";
    summer_stone.loadTexture();
    summer_water.path = "img/summer/summer_water.png";
    summer_water.loadTexture();
    summer_grass.path = "img/summer/summer_grass.png";
    summer_grass.loadTexture();

    // Autumn cells
    autumn_dirt.path = "img/autumn/autumn_dirt.png";
    autumn_dirt.loadTexture();
    autumn_grass.path = "img/autumn/autumn_grass.png";
    autumn_grass.loadTexture();
    autumn_stone.path = "img/autumn/autumn_stone.png";
    autumn_stone.loadTexture();
    autumn_water.path = "img/autumn/autumn_water.png";
    autumn_water.loadTexture();

    // Winter cells
    winter_dirt.path = "img/winter/winter_dirt.png";
    winter_dirt.loadTexture();
    winter_grass.path = "img/winter/winter_grass.png";
    winter_grass.loadTexture();
    winter_stone.path = "img/winter/winter_stone.png";
    winter_stone.loadTexture();
    winter_water.path = "img/winter/winter_water.png";
    winter_water.loadTexture();

    //Units
    tank.path = "img/units/tank.png";
    tank.loadTexture();
    soldier.path = "img/units/soldier.png";
    soldier.loadTexture();

    // Interface
    cellCursor.path = "img/interface/cell_cursor.png";
    cellCursor.loadTexture();
}

