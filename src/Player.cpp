#include <iostream>
#include "../include/Player.hpp"
#include "../include/Cell.hpp"
#include "../include/Const.hpp"
#include "../include/Game.hpp"
using namespace std;

// Constructor declaration
Player::Player(int a, string b, int e, int _color [3])
{
    id = a;
    name = b;
    money = 30;
    currentAction = NULL;
    r = _color[0];
    g = _color[1];
    b = _color[2];
};

bool Player::playerIsDead() {
    if (units.size() < 1)
    {
        // cout << "-> " << name << " en mort !" << endl;
        return false;
    } else {
        // cout << "-> " << name << " en en vie !" << endl;
        return true;
    }
    
}

// 
void Player::selectUnit(SDL_Event* e)
{
    // cout << "selectUnit" << endl;
    // Selectionner une de ses unités
    for (size_t i = 0; i < units.size(); i++)
    {
        if (units[i]->isClicked(e))
        {   
            if (e != NULL && e->type == SDL_MOUSEBUTTONUP && e->button.button == SDL_BUTTON_LEFT)
            {
                cout << "Clic gauche sur unit -> New PlayerMove" << endl;
                currentAction = new PlayerMove(this, units[i]);
            }
            if (e != NULL && e->type == SDL_MOUSEBUTTONUP && e->button.button == SDL_BUTTON_RIGHT)
            {
                currentAction = new PlayerAttack(this, units[i]);
                cout << "Clic droit sur unit -> New PlayerAttack" << endl;
            }
        }
    }
};


bool Player::playerTurn(SDL_Event* e) {

    selectUnit(e); // Le joueur selectione une de ses unités et commence une action (attaquer ou déplacer)
    // Si une action est commencée, on essaie de l'executer

    if (currentAction != NULL)
    {
        // Vérification que l'action n'a pas déjà été effectuée avec cette unité
        if (currentAction->type == "move" && currentAction->attacker->hasMoved)
        {
            currentAction = NULL;
            cout << "// CETTE UNITÉ A DÉJÀ ÉTÉ DÉPLACÉE //" << endl;
            return true;
        } else if (currentAction->type == "attack" && currentAction->attacker->hasAttack)
        {
            currentAction = NULL;
            cout << "// CETTE UNITÉ A DÉJÀ ATTAQUÉ //" << endl;
            return true;
        }
        
        // Si l'action est un déplacement mais que la destination n'est pas encore définie, on selectionne une destination
        if (currentAction->isSet() == false)
        {
            currentAction->selectTarget(e);
            return true;
        }
        if (currentAction->isSet() == true) {
            // ANIMATION DU SHOT
            // if (currentAction->animate())
            // {
            //     currentAction->execute();
            //     currentAction = NULL;
            // }
            currentAction->execute();
            currentAction = NULL;
        }
    }

    // Est ce que la partie est finie ?
    if (game->isWinner())
    {
        cout << "isWinner() returned true" << endl;
        return false;
    }

    // Est ce que le joueur à terminer son tour ?
    // == est ce que toutes les unités ont déjà bougé et attaqué ?
    for (uint i = 0; i < units.size(); i++)
    {
        if (units[i]->isTurnDone())
        {
            continue;
        } else {
            return true;
        }
    };
    // Si on n'a pas trouvé d'unité qui puisse encore faire des actions, on réinitialise les actions de toutes le unités avant de retourner false
    for (uint i = 0; i < units.size(); i++)
    {
        units[i]->resetUnitTurn();
    };
    return false;
}

// ----------------------------------------------
// PLAYERACTION ---------------------------------

PlayerAction::PlayerAction(){
    //
};

PlayerAction::~PlayerAction(){
    //
};

void PlayerAction::execute() {
    cout << "execute de playerAction" << endl;
}

void PlayerAction::selectTarget(SDL_Event* e) {
    cout << "selectTarget de playerAction" << endl;
};

bool PlayerAction::isSet() {
    cout << "PlayerAction isSet() method call" << endl;
}

bool PlayerAction::animate(){
    return true;
}; // Animation de l'action

// ----------------------------------------------
// PLAYERMOVE ----------

PlayerMove::PlayerMove(Player* _player, Unit* _attacker) {
    player = _player;
    attacker = _attacker;
    type = "move";
    destination = NULL;
    cout << "newmove type : "<< type << endl;
};

PlayerMove::~PlayerMove() {
    cout << "Deleting PlayerMove" << endl;
};

void PlayerMove::execute() {
    cout << "execute de playerMove" << endl;
    attacker->moveUnit(destination);
    delete this;
}

bool PlayerMove::animate(){
    cout << "PlayerMove::animate()" << endl;
    return true;  
};


void PlayerMove::selectTarget(SDL_Event* e) {
    // cout << "selectTarget de playerMove" << endl;

    // Détecter le clic sur une case
    for (int i = 0; i < MAP_SIZE; i++)
    {
        for (int j = 0; j < MAP_SIZE; j++)
        {
            if (player->game->map[i][j]->isClicked(e))
            {
                cout << "clic sur case : " << i << " " << j << endl;
                // Détecter si la case est bien vide et traversable
                if (player->game->map[i][j]->unit == NULL && player->game->map[i][j]->moveThrough())
                {
                    // cout << "La case est libre" << endl;
                    destination = player->game->map[i][j];
                    cout << "Destination set" << endl;
                } 
                if (player->game->map[i][j]->unit != NULL) {
                    cout << "Case occupée !" << endl;
                } else {
                    cout << "obstacle :/" << endl;
                }
            }
        }  
    }
};

bool PlayerMove::isSet() {
    if (destination != NULL)
    {
        // cout << "Destination de Move is set" << endl;
        return true;
    } else {
        return false;
    }
};

// ----------------------------------------------
// PLAYERATTACK ----------

PlayerAttack::PlayerAttack(Player* _player, Unit* _unit) {
    player = _player;
    attacker = _unit;
    type = "attack";
    target = NULL;
    shot = NULL;
    cout << "newattack type : "<< type << endl;
};

void PlayerAttack::execute() {
    cout << "execute de playerAttack" << endl;
    attacker->attackUnit(target);
    delete this;
};

bool PlayerAttack::animate(){
    cout << "PlayerAttack::animate()" << endl;
    if (shot == NULL)
    {
        float ax = attacker->cell->x*(WINDOW_WIDTH/MAP_SIZE);
        float ay = attacker->cell->y*(WINDOW_HEIGHT/MAP_SIZE);
        float tx = target->cell->x*(WINDOW_WIDTH/MAP_SIZE);
        float ty = target->cell->y*(WINDOW_HEIGHT/MAP_SIZE);
        shot = new Shot("img/sprite.png", 32, 32, ax , ay, tx, ty, 50., 5);
        cout << " » Creating Shot" << endl;
    };
    if (!shot->finished) {
        cout << " » Shot isn't finished" << endl;
        shot->displayShot();
        return false;
    } else {
        cout << "delete shot" << endl;
        delete shot;
        return true; 
    } 
};


void PlayerAttack::selectTarget(SDL_Event* e) {
    // cout << "selectTarget de playerAttack" << endl;

    // Détecter le clic sur une case
    for (int i = 0; i < MAP_SIZE; i++)
    {
        for (int j = 0; j < MAP_SIZE; j++)
        {
            if (player->game->map[i][j]->isClicked(e))
            {
                cout << "clic sur case : " << i << " " << j << endl;
                // Détecter si la case a bien une unité qui n'est pas celle qui attaque
                if (player->game->map[i][j]->unit != NULL && player->game->map[i][j]->unit != attacker)
                {
                    target = player->game->map[i][j]->unit;
                    cout << "Target set" << endl;
                } else {
                    cout << "Ce n'est pas une unité !" << endl;
                }
            }
        }  
    }
};

bool PlayerAttack::isSet() {
    if (target != NULL && target != attacker)
    {
        // cout << "Target de Attack set" << endl;
        return true;
    } else {
        return false;
    }
};

void Player::buyUnits(SDL_Event* e) {
    // Selectionner une unité
    int nb_units = 3;
    Player* player = this;
    Fantassin* fantassin = new Fantassin(this->game->map[0][0], &soldier, nullptr);
    Archer* archer = new Archer(this->game->map[0][1], &soldier, nullptr);
    Knight* knight = new Knight(this->game->map[0][2], &tank, nullptr);
    Unit* bank[] = {fantassin, archer, knight};
    for (size_t i = 0; i < nb_units; i++)
    {
        if (bank[i]->isClicked(e))
        {   
            if (e != NULL && e->type == SDL_MOUSEBUTTONUP)
            {
                if (this->money >= bank[i]->cost) {
                    cout << "Clic gauche sur unit -> nouvelle unité" << endl;
                    this->units.push_back(bank[i]);
                    this->money -= bank[i]->cost;
                } else {
                    cout << "plus d'argent :/" << endl;
                }
            }
        }
    }
};


// void Player::playerTurn(SDL_Event e) {
// if (selectedUnit != 0) {
//     for (size_t i = 0; i < units.size(); i++) {
//         if (units[i]->cell->isClicked(e))
//         {
//             selectedUnit = units[i];
//             break;
//         }
//     }
// } else {
//     cout << "Unit is selected " << selectedUnit->cell->x << " " << selectedUnit->cell->y << endl;
// }
// if (selectedUnit == -1) {
//     for (size_t i = 0; i < units.size(); i++) {
//         if (units[i]->cell->isClicked(e))
//         {
//             cout << "Unité sélectionnée" << endl;
//             selectedUnit = i;
//             break;
//         }
//     }
// } else if (selectedUnit != -1 && e.button.y <= WINDOW_HEIGHT && e.type == SDL_MOUSEBUTTONDOWN) {
//     // cout << "UNIT IS SELECTED" << endl;
//     int cellX = e.button.x/(WINDOW_HEIGHT/MAP_SIZE);
//     int cellY = e.button.y/(WINDOW_HEIGHT/MAP_SIZE);
//     cout << "selected unit is : " << cellX << " " << cellY << endl;
//     game->map[cellX][cellY]->unit->cell = game->map[cellX][cellY];
//     game->map[cellX][cellY]->unit = units[selectedUnit];
// }
// }

// bool Player::playerIsDead(Player* player) {
//     for (int i = 0; i < (int)player->unites.size(); i++)
//     {
//         if (player->unites[i].life > 0)
//         {
//             return false;
//         }
//     }
//     cout << player->name << " has no more units !\n" << player->enemy->name << " Congrats ! you win" << endl;
//     endGame(player);
//     return true;
// }

// bool Player::playerAttack(Player* player) {
//     Unit* unitAtt;
//     Unit* unitCible;
//     bool ok = false;

//     while (ok == false)
//     {
//         cout << player->name << " you're attacking '" << "\n" << "the attacked unit : ";
//         unitAtt = selectUnit(player);

//         if (unitAtt == NULL)
//         {
//             break;
//         }

//         if (unitAtt->life > 0)
//         {
//             while (unitCible->life <= 0)
//             {
//                 cout << "targeted UNIT: ";
//                 unitCible = selectUnit(player->enemy);

//                 if (unitCible == NULL)
//                 {
//                     ok = true;
//                     break;
//                 }
//             }

//             if (unitCible != NULL)
//             {
//                 while (nombreDeCases(unitAtt->posX, unitAtt->posY, unitCible->posX, unitCible->posY) > unitAtt->range)
//                 {
//                     if (nombreDeCases(unitAtt->posX, unitAtt->posY, unitCible->posX, unitCible->posY) > unitAtt->range) {
//                         cout << "you're so far from the enamy ,try again ! " << endl;
//                         playerAttack(player);
//                     }
//                 }

//                 unitAttackUnit(unitAtt, unitCible);

//                 ok = true;
//                 return true;
//             }
//             else {
//                 ok = true;
//                 break;
//             }
//         }
//         else {
//             cout << "Dead unit :/ " << endl;
//         }
//     }
// }

// // setters & getters name
// void Player::setName(string name) {
//     this->name = name;
// }
// string Player::getName() {
//     return this->name;
// }

// // setters & getters units
// void Player::setUnits(vector<Unit> units) {
//     this->units = units;
// }
// string getUnits() {
//     return this->units;
// }

// // setters & getters color
// void Player::setColor(string color) {
//     if (color == "blue" || color == "red"  ) {
//         this->color = color;
//     }
//     else {
//         this->color = "blue";
//     }
// }
// string Player::getColor() {
//     return this->color;
// }

// // setters & getters money
// void Player::setMoney(int money) {
//     this->money = money;
// }
// string Player::getMoney() {
//     return this->money;
// }

