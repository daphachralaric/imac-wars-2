#include "include/Include.hpp"
#include "include/Const.hpp"
#include <stdlib.h>
#include <time.h>  
using namespace std;

static float aspectRatio;

/* Espace fenetre virtuelle */
static const float GL_VIEW_SIZE =  WINDOW_HEIGHT;

/* Nombre de bits par pixel de la fenetre */
static const unsigned int BIT_PER_PIXEL = 32;

/* Nombre minimal de millisecondes separant le rendu de deux images */
static const Uint32 FRAMERATE_MILLISECONDS = 1000 / 60;

// CRÉATION DE LA GAME ------------------
    Game game;

// État du jeu
int state = 0;

void reshape(SDL_Surface** surface, unsigned int width, unsigned int height)
{ 
    SDL_Surface* surface_temp = SDL_SetVideoMode(   
        width, height, BIT_PER_PIXEL,
        SDL_OPENGL | SDL_GL_DOUBLEBUFFER | SDL_RESIZABLE);
    if(NULL == surface_temp) 
    {
        fprintf(
            stderr, 
            "Erreur lors du redimensionnement de la fenetre.\n");
        exit(EXIT_FAILURE);
    }
    *surface = surface_temp;

    aspectRatio = width / (float) height;

    glViewport(0, 0, (*surface)->w, (*surface)->h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // if( aspectRatio > 1) 
    // {
    //     gluOrtho2D(0, WINDOW_WIDTH, WINDOW_HEIGHT, 0);
    // }
    // else
    // {
    //     gluOrtho2D(
    //     -GL_VIEW_SIZE / 2., GL_VIEW_SIZE / 2.,
    //     -GL_VIEW_SIZE / 2. / aspectRatio, GL_VIEW_SIZE / 2. / aspectRatio);
    // }
}

//fonction appelée quand on appuie sur start
void bonsoir() {
    cout << "start called" << endl;
    state = 1;      //redirige sur acheter des unités
    game.initGame(); // Initialise une nouvelle game
}

//fonction appelée quand on appuie sur buy
void bonsoir2() {
    cout << "acheter des unités called" << endl;
    state = 3;      //permet d'acheter des unités
}

//fonction appelée quand on appuie sur enter
void bonsoir3() {
    cout << "démarrer partie called" << endl;
    state = 1;      //démarre la partie
}

int main(int argc, char** argv) 
{
    srand (time(NULL));
    /* Initialisation de la SDL */
    if(-1 == SDL_Init(SDL_INIT_VIDEO)) 
    {
        fprintf(
            stderr, 
            "Impossible d'initialiser la SDL. Fin du programme.\n");
        exit(EXIT_FAILURE);
    }

    // Initialisation de SDL_TTF
    if(TTF_Init() == -1)
    {
        fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }
    
    /* Ouverture d'une fenetre et creation d'un contexte OpenGL */
    SDL_Surface* surface;
    reshape(&surface, WINDOW_WIDTH, WINDOW_HEIGHT);

    /* Initialisation du titre de la fenetre */
	SDL_WM_SetCaption(WINDOW_TITLE, NULL);

    // Charge les textures globales
    initTextures();


    // GESTION DU TEXTE
    Text montexte(SummitAttack, 65, SDL_Color{0, 0, 255}, "Noraj de l'OpenGLage");
    Text buy(SummitAttack, 35, SDL_Color{255,255, 0}, "Buy units");

    // VARIABLES DES TEXTURES ---------------------------------------
    Texture hover("img/hover.png");
    Texture hoverDown("img/hoverDown.png");
    // int mouseX, mouseY;
    // bool mouseDown = false;
    Shot fire("img/sprite.png", 32, 32, 328, 234, 12, 714, 50., 2);


    // CRÉATION D'UN JOUEUR
    // Player player1(1, "Bob", 0);
    // cout << "player1 OK, creating player2" << endl;
    // Player player2(1, "Michel", 0);
    // cout << "player2 created" << endl;

    // game.setPlayer(&player1);
    // game.setPlayer(&player2);

    // // // CRÉATION D'UNE UNITÉ
    // Unit unite(game.map[0][0], &tank, &player1);
    // Unit unit2(game.map[4][5], &soldier, &player2);

    // player1.units.push_back(&unite);
    // player2.units.push_back(&unit2);

    // player1.units.push_back(&fanstassin);
    // player2.units.push_back(&unit2);

    // BOUTON ---------------------------
    int r[3] = {255, 0, 0};
    int g[3] = {0, 255, 0};
    int c[3] = {0, 0, 255};
    Button mybutton((WINDOW_WIDTH/2)-100, (WINDOW_HEIGHT/2)-50, 100, 200, r, g, c, SummitAttack, 35, SDL_Color{255, 255, 255}, "START");
    cout << "color std : " << mybutton.colorStandard[0] << " " << mybutton.colorStandard[1] << " " << mybutton.colorStandard[2] << endl;

    mybutton.onClick = &bonsoir;

    // Button buyTank((3*WINDOW_WIDTH/4), (WINDOW_HEIGHT/2-150), 100, 200, r, g, c, SummitAttack, 35, SDL_Color{255, 255, 255}, "BUY TANK");
    // Button buySoldier((3*WINDOW_WIDTH/4), (WINDOW_HEIGHT/2), 100, 200, r, g, c, SummitAttack, 35, SDL_Color{255, 255, 255}, "BUY SOLDIER");
    // Button Enter((3*WINDOW_WIDTH/4), (WINDOW_HEIGHT/2 + 200), 100, 200, r, g, c, SummitAttack, 35, SDL_Color{255, 255, 255}, "ENTER");
  
    // // INTERFACE -------------------------
    Interface start;        //interface du menu start
    start.buttonsList.push_back(&mybutton);     //bouton start
    Texture bg("img/home_bg.png");
    auto t = make_tuple(&bg, 0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    start.texturesList.push_back(t);

    // Interface buyUnits;     //interface pour acheter des unités
    // Texture unit1("img/units/tank.png");
    // Texture unit2("img/units/soldier.png");
    // buyUnits.buttonsList.push_back(&buyTank);     //bouton buyTank
    // buyUnits.buttonsList.push_back(&buySoldier);     //bouton buySoldier
    // buyUnits.buttonsList.push_back(&Enter);         //bouton enter
    // auto tup1 = make_tuple(&unit1, 3*WINDOW_WIDTH/4, 100, 300, 100);
    // auto tup2 = make_tuple(&unit2, 3*WINDOW_WIDTH/4, 250, 300, 100);
    // buyUnits.texturesList.push_back(tup1);
    // buyUnits.texturesList.push_back(tup2);


    // EVENEMENTS ------------
    SDL_Event e;
    SDL_Event* event = NULL;

    /* Boucle principale */
    int loop = 1;
    while(loop) 
    {
        event = NULL;

        /* Boucle traitant les evenements */
        while(SDL_PollEvent(&e))
        {
            event = &e;

            /* L'utilisateur ferme la fenetre : */
			if(e.type == SDL_QUIT) 
			{
				loop = 0;
				break;
			}
		
			if(	e.type == SDL_KEYDOWN 
				&& (e.key.keysym.sym == SDLK_q || e.key.keysym.sym == SDLK_ESCAPE))
			{
                // Suppression de toutes les textures et texts
                for (size_t i = 0; i < loadedTextures.size(); i++)
                {
                    loadedTextures[i]->deleteTexture();
                    cout << " × " << loadedTextures.size() - i << "textures left to delete \n";
                }
                for (size_t i = 0; i < loadedTexts.size(); i++)
                {
                    loadedTexts[i]->deleteText();
                    cout << " × " << loadedTexts.size() - i << " texts left to delete" << endl;
                }
                loop = 0;

				break;
			}
            
            /* Quelques exemples de traitement d'evenements : */
            switch(e.type) 
            {
                /* Redimensionnement fenetre */
				case SDL_VIDEORESIZE:
                    reshape(&surface, e.resize.w, e.resize.h);
                    break;

                /* Clic souris */
                case SDL_MOUSEBUTTONUP:
                    e.button.x *= (float)WINDOW_WIDTH/surface->w;
                    e.button.y *= (float)WINDOW_HEIGHT/surface->h;
                    // Le bouton n'est plus enfoncé
                    //mouseDown = false;
                    // Récupérer la case cliquée
                    // Changer la texture de la case cliquée
                    if (e.button.x < WINDOW_HEIGHT)
                    {
                        cout << "Clic sur la map" << endl;
                    }
                    break;

                /* Clic souris enfoncé */
                case SDL_MOUSEBUTTONDOWN:
                    e.button.x *= (float)WINDOW_WIDTH/surface->w;
                    e.button.y *= (float)WINDOW_HEIGHT/surface->h;
                    cout << "Mouse Down" << endl;
                    // mouseDown = true;
                    break;
                
                /* Touche clavier */
                case SDL_KEYDOWN:
                    printf("touche pressee (code = %d)\n", e.key.keysym.sym);
                    break;

                case SDL_MOUSEMOTION:
                    // On récupère les coordonées de la souris
                    e.button.x *= (float)WINDOW_WIDTH/surface->w;
                    e.button.y *= (float)WINDOW_HEIGHT/surface->h;
                    break;
                    
                default:
                    break;
            }
            
        }

        /* Recuperation du temps au debut de la boucle */
        Uint32 startTime = SDL_GetTicks();
        
        /* Placer ici le code de dessin */
        glClear(GL_COLOR_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glPushMatrix();

            if (state == 0) {
                // AFFICHAGE DE L'INTERFACE START
                start.displayInterface(event);
                mybutton.onClick = &bonsoir;

            } 

            // else if (state == 2) {
            //     // AFFICHAGE DE L'INTERFACE BUY UNITS
            //     // buyUnits.displayInterface(event);
            //     // buy.displayText(3*WINDOW_WIDTH/4, 60);
            //     // buyTank.onClick = &bonsoir2;
            //     // buySoldier.onClick = &bonsoir2;
            //     // Enter.onClick = &bonsoir3;

            // }

            // else if (state == 3) {
            //     //ACHETER DES UNITES
            //     // buyUnits.displayInterface(event);
            //     // buy.displayText(3*WINDOW_WIDTH/4, 60);
                

            // }
            
            else if (state == 1)
            {
                // JOUER LA PARTIE
                switch (game.playGame(event))
                {
                case 1:
                    // La partie se joue, oklm
                    break;
                case 0:
                    // retour au menu start
                    // Réinitialisation de la partie
                    game.cleanGame();
                    
                    state = 0;
                default:
                    break;
                }            
                                

                // Si le clic est actuellement pressé
                // if (mouseDown)
                // {
                //     hoverDown.displayTexture((WINDOW_HEIGHT/MAP_SIZE)*(mouseX/(WINDOW_HEIGHT/MAP_SIZE)), (WINDOW_HEIGHT/MAP_SIZE)*(mouseY/(WINDOW_HEIGHT/MAP_SIZE)), (WINDOW_HEIGHT/MAP_SIZE), (WINDOW_HEIGHT/MAP_SIZE), e);
                // }

                // Écriture du Texte
                // montexte.displayText(100, 100);

                // ANIMATION SPRITE EN MOUVEMENT
                // if (fire.finished == false)
                // {
                //     fire.displayShot();

                // } 
            }

            // Cercle autour de la case pointée
            // int range = 4;
            // int caseX = mouseX/(WINDOW_HEIGHT/MAP_SIZE);
            // int caseY = mouseY/(WINDOW_HEIGHT/MAP_SIZE);
            // for (int i = -range; i <= range; i++)
            // {
            //     for (int j = -range; j <= range; j++)
            //     {
            //         if ((i+j) <= range && (i+j) >= -range && (i-j) <= range && (i-j) >= -range && !(i==0 && j==0))
            //         {
            //             hoverDown.displayTexture(((WINDOW_HEIGHT/MAP_SIZE)*(caseX))+(i*(WINDOW_HEIGHT/MAP_SIZE)), ((WINDOW_HEIGHT/MAP_SIZE)*(caseY))+(j*(WINDOW_HEIGHT/MAP_SIZE)), (WINDOW_HEIGHT/MAP_SIZE), e);
            //         }
            //     }
            // }
            
            // Tooltip
            // if (mouseX < WINDOW_HEIGHT) {
                // Text tooltip(SummitAttack, 25, SDL_Color{255, 255, 255}, game.map[mouseX/(WINDOW_HEIGHT/MAP_SIZE)][mouseY/(WINDOW_HEIGHT/MAP_SIZE)].name.c_str());
                // tooltip.displayText(mouseX, mouseY - 30);
            //};            
            
        glPopMatrix();

        /* Echange du front et du back buffer : mise a jour de la fenetre */
        SDL_GL_SwapBuffers();

        /* Calcul du temps ecoule */
        Uint32 elapsedTime = SDL_GetTicks() - startTime;
        /* Si trop peu de temps s'est ecoule, on met en pause le programme */
        if(elapsedTime < FRAMERATE_MILLISECONDS) 
        {
            SDL_Delay(FRAMERATE_MILLISECONDS - elapsedTime);
        }
    }

    /* Liberation des ressources associees a la SDL */ 
    //TTF_CloseFont(police); /* Doit être avant TTF_Quit() */
    TTF_Quit();
    SDL_Quit();
    
    return EXIT_SUCCESS;
}
